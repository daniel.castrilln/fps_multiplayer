using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gun : MonoBehaviour
{
    #region Variables
    public Gun_Data data;
    public Transform muzzlePoint;
    public AudioClip gunShot;
    public AudioClip reload;
    public AudioClip weaponChange;
    #endregion

    #region Unity Functions
    private void Awake()
    {
        data.actualAmmo = data.maxGunAmmo;

    }
    #endregion
}

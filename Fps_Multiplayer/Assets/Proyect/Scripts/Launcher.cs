using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using TMPro;
using Photon.Realtime;

public class Launcher : MonoBehaviourPunCallbacks
{
    #region Variables
    public static Launcher Instance;
    [SerializeField] private GameObject []screenObjects;
    
    [SerializeField] private TMP_Text infoText;
    [SerializeField] private TMP_Text roomText;
    [SerializeField] private TMP_Text errorText;

    [SerializeField] TMP_InputField roomNameInput;

    [SerializeField] GameObject buttonPref;
    [SerializeField] Transform scrollContent;
    [SerializeField] List<RoomButton> roomButtonsList = new List<RoomButton>();

    #endregion

    #region Unity Functions

    private void Start()
    {
        infoText.text = "Conecting to Network....";
        SetScreenObjects(0);
        PhotonNetwork.ConnectUsingSettings();
    }
    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(this);
        }
        
    }
    #endregion

    #region Photon
    public override void OnConnectedToMaster()
    {
        PhotonNetwork.JoinLobby();
        SetScreenObjects(1);
    }
    public override void OnJoinedRoom()
    {
        roomText.text = PhotonNetwork.CurrentRoom.Name;
        SetScreenObjects(3);
    }
    public override void OnCreateRoomFailed(short returnCode, string message)
    {
        errorText.text = "Ya existe una sala con este nombre..   " + message;
        SetScreenObjects(4);
    }

    public override void OnRoomListUpdate(List<RoomInfo> roomList)
    {
        for (int i = 0; i < roomList.Count; i++)
        {
            if (roomList[i].RemovedFromList)
            {
                for (int j = 0; j < roomButtonsList.Count; j++)
                {
                    if (roomList[i].Name == roomButtonsList[j].roomInfo.Name)
                    {
                        GameObject go = roomButtonsList[j].gameObject;
                        roomButtonsList.Remove(roomButtonsList[j]);
                        Destroy(go);
                    }
                }
            }
        }
        for (int i = 0; i < roomList.Count; i++)
        {
            if (roomList[i].PlayerCount != roomList[i].MaxPlayers && !roomList[i].RemovedFromList)
            {
                RoomButton newRoomButton = Instantiate(buttonPref, scrollContent).GetComponent<RoomButton>();
                newRoomButton.SetButtonDetails(roomList[i]);
                roomButtonsList.Add(newRoomButton);
                Debug.Log(roomList);
            }
        }
    }

    public void JoinRoom(RoomInfo info)
    {
        PhotonNetwork.JoinRoom(info.Name);
        infoText.text = "Joining Room...";
        SetScreenObjects(index: 0);
    }

    #endregion

    #region Custom Functions
    public void SetScreenObjects(int index)
    {
        for (int i = 0; i < screenObjects.Length; i++)
        {
            screenObjects[i].SetActive(i == index);
        }
    }

    public void CreateRoom()
    {
        if (!string.IsNullOrEmpty(roomNameInput.text))
        {
            RoomOptions options = new RoomOptions();
            options.MaxPlayers = 8;
            PhotonNetwork.CreateRoom(roomNameInput.text);
            infoText.text = "Creating Room....";
            SetScreenObjects(0);
        }
    }

    public void LeaveRoom()
    {
        PhotonNetwork.LeaveRoom();
        SetScreenObjects(1);
    }
    #endregion
}

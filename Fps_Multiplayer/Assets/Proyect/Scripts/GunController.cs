using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.VFX;

public class GunController : MonoBehaviour
{
    #region Variables   
    public VisualEffect MuzzleFlash;

    public Player_Controller player_Controller;
    public Gun[] guns;
    public Gun actualGun;
    public int indexGun = 0;
    public int maxGun = 3;

    float lastShotTime = 0;
    float lastChangeTime = 0;
    float changeTime = 0;
    Vector3 currentRotation;
    Vector3 targetRotation;
    public float returnSpeed;
    public float snappines;
    
    private float lastReload;
    private bool isReloading;
    private bool isChanging;

    public GameObject prefabBulletHole;
    public float distance;
    public LayerMask lm;

    #endregion

    #region Unity Functions
    private void Update()
    {
        if (Input.GetButtonDown("E") && Physics.Raycast(player_Controller.cam.transform.position, player_Controller.cam.transform.forward, out RaycastHit hit,distance, lm))
        {
            Debug.Log("Recogido");
            Transform gun = hit.transform;
            if (hit.transform != null)
            {
                Interaction(gun, indexGun);
            }

        }

        if (actualGun != null)
        {
            if (lastShotTime <= 0 && !isReloading)
            {
                
                if (!actualGun.data.automatic)
                {
                    if (Input.GetButtonDown("Fire1"))
                    {
                        if (actualGun.data.actualAmmo > 0)
                        {
                            Shoot();
                        }
                    }
                }
                else
                {
                    if (Input.GetButton("Fire1"))
                    {
                        if (actualGun.data.actualAmmo > 0)
                        {
                            Shoot();
                        }
                    }
                }

                if (Input.GetButtonDown("Reload") && !isReloading)
                {
                    if (actualGun.data.actualAmmo < actualGun.data.maxGunAmmo)
                    {
                        Debug.Log("reload");
                        lastReload = 0;
                        isReloading = true;
                        AudioSource.PlayClipAtPoint(actualGun.reload, gameObject.transform.position);
                    }
                }

            }


        }

        else
        {

        }

        if (lastShotTime >= 0)
        {
            lastShotTime -= Time.deltaTime;
        }

        if (isReloading)
        {
            lastReload += Time.deltaTime;
            if (lastReload >= actualGun.data.reloadTime)
            {
                isReloading = false;
                Reload();
                
            }
        }

        targetRotation = Vector3.Lerp(targetRotation, Vector3.zero, returnSpeed * Time.deltaTime);
        currentRotation = Vector3.Slerp(currentRotation, targetRotation, snappines * Time.deltaTime);
        player_Controller.recoil.localRotation = Quaternion.Euler(currentRotation);
        ChangeGun(indexGun);

        if (Input.GetButtonDown("Gun1") && !isReloading)
        {
            if (indexGun != 0)
            {
                
                indexGun = 0;
                lastChangeTime = 0;
                if (actualGun != null)
                {
                    AudioSource.PlayClipAtPoint(actualGun.weaponChange, gameObject.transform.position);
                    actualGun.gameObject.SetActive(false);
                    actualGun = null;
                }
                isChanging = true;
            }
        }

        if (Input.GetButtonDown("Gun2") && !isReloading)
        {
            if (indexGun != 1)
            {

                indexGun = 1;
                lastChangeTime = 0;
                if (actualGun != null)
                {
                    AudioSource.PlayClipAtPoint(actualGun.weaponChange, gameObject.transform.position);
                    actualGun.gameObject.SetActive(false);
                    actualGun = null;
                }
                isChanging = true;
            }
        }

        if (Input.GetButtonDown("Gun3") && !isReloading)
        {
            if (indexGun != 2)
            {
               
                indexGun = 2;
                lastChangeTime = 0;
                if (actualGun != null)
                {
                    AudioSource.PlayClipAtPoint(actualGun.weaponChange, gameObject.transform.position);
                    actualGun.gameObject.SetActive(false);
                    actualGun = null;
                }
                isChanging = true;
            }
        }
        if (isChanging)
        {
            lastChangeTime += Time.deltaTime;
            if (lastChangeTime >= changeTime)
            {
                isChanging = false;
                ChangeGun(indexGun);  
            }
        }

    }
    #endregion

    #region Custom Functions
    private void Shoot()
    {
        if (Physics.Raycast(player_Controller.cam.transform.position, player_Controller.cam.transform.forward, out RaycastHit hit, actualGun.data.range))
        {
            if (hit.transform != null)
            {
                Debug.Log($"We shooting at: {hit.transform.name }");
                GameObject bulletHole = Instantiate(prefabBulletHole, hit.point + hit.normal * 0.01f, Quaternion.LookRotation(hit.normal, Vector3.up));
                Destroy(bulletHole,10f);
                
            }
            
        }
        actualGun.data.actualAmmo--;
        lastShotTime = actualGun.data.fireRate;
        AddRecoil();
        GameObject MuzzleVfx = VisualEffect.Instantiate(MuzzleFlash,actualGun.muzzlePoint.position,actualGun.muzzlePoint.rotation).gameObject;
        MuzzleVfx.transform.parent = actualGun.muzzlePoint;
        Destroy(MuzzleVfx, 1f);
        AudioSource.PlayClipAtPoint(actualGun.gunShot, gameObject.transform.position);
    }
    void Reload() 
    {
        actualGun.data.actualAmmo = actualGun.data.maxGunAmmo;
    }

    void AddRecoil() 
    {
        targetRotation -= new Vector3(actualGun.data.recoil.x, Random.Range(-actualGun.data.recoil.y, actualGun.data.recoil.y),0f); 
    }
    private void ChangeGun(int index)
    {
        if (guns[index] != null)
        {
            actualGun = null;
            actualGun = guns[index];
            actualGun.gameObject.SetActive(true);
        }
    }

    void Interaction(Transform floor_Gun, int index)
    {
        if (actualGun!= null)
        {
            ReleasedGun(actualGun, indexGun);

        }

        Rigidbody rb = floor_Gun.GetComponent<Rigidbody>();
        rb.isKinematic = true;
        floor_Gun.transform.parent = player_Controller.gunPoint;
        actualGun = floor_Gun.transform.gameObject.GetComponent<Gun>();
        floor_Gun.transform.localPosition = actualGun.data.offset;
        floor_Gun.transform.localRotation = Quaternion.identity;
        guns[index] = actualGun;


    }

    void ReleasedGun(Gun floor_Gun, int index)
    {
        Rigidbody rb = floor_Gun.GetComponent<Rigidbody>();
        actualGun.transform.parent = null;
        guns[index] = null;
        actualGun = null;
        rb.isKinematic = false;
        rb.AddForce(floor_Gun.transform.up * 2, ForceMode.Impulse);
        rb.AddTorque(floor_Gun.transform.right * 2, ForceMode.Impulse);
    }

    #endregion
}

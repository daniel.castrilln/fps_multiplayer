using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player_Controller : MonoBehaviour
{
    #region Variables
    public Camera cam;
    public Transform recoil;
    public Transform gunPoint;
    [SerializeField] private CharacterController controller;
    [SerializeField] private Transform pointOfView;

    [SerializeField] private float walkSpeed = 5f;
    [SerializeField] private float runSpeed = 0f;
    [SerializeField] private float jumpForce = 12f;
    [SerializeField] private float gravityMod;

    private float actualSpeed;
    private float horizontalRotationStore;
    private float verticalRotationStore;
    public Vector2 mouseInput;
    private Vector3 direction;
    private Vector3 movement;

    [Header("Ground Detection")]
    [SerializeField] private bool isgrounded;
    [SerializeField] private float radio;
    [SerializeField] private float distance;
    [SerializeField] private Vector3 offset;
    [SerializeField] private LayerMask lm;

    #endregion

    #region Unity Functions
    private void Start()
    {
        Cursor.lockState = CursorLockMode.Locked; // Esto sirve para poder ocultar el cursor
        cam = Camera.main;

    }
    private void Update()
    {
        Rotation();
        Movement();
    }

    private void LateUpdate()
    {
        cam.transform.position = recoil.position;
        cam.transform.rotation = recoil.rotation;

        gunPoint.transform.position = recoil.position;
        gunPoint.transform.rotation = recoil.rotation;
    }

    private void OnDrawGizmos()
    {
        Gizmos.DrawWireSphere(transform.position + offset, radio);
        if (Physics.SphereCast(transform.position + offset, radio, Vector3.down, out RaycastHit hit, distance, lm))
        {
            Gizmos.color = Color.green;
            Vector3 endPoint = (transform.position + offset) + (Vector3.down * distance);
            Gizmos.DrawWireSphere(endPoint, radio);
            Gizmos.DrawLine(transform.position + offset, endPoint);

            Gizmos.DrawSphere(hit.point, 0.1f);
        }
        else
        {
            Gizmos.color = Color.red;
            Vector3 endPoint = (transform.position + offset) + (Vector3.down * distance);
            Gizmos.DrawWireSphere(endPoint, radio);
            Gizmos.DrawLine(transform.position + offset, endPoint);
        }
    }
    #endregion

    #region Custom Funcions
    private void Rotation()
    {
        mouseInput = new Vector2(Input.GetAxisRaw("Mouse X"), Input.GetAxisRaw("Mouse Y"));

        horizontalRotationStore += mouseInput.x;
        verticalRotationStore -= mouseInput.y;

        verticalRotationStore = Mathf.Clamp(verticalRotationStore, -60, 60f);

        transform.rotation = Quaternion.Euler(0, horizontalRotationStore, 0f);
        pointOfView.transform.localRotation = Quaternion.Euler(verticalRotationStore, transform.rotation.y, 0f);
    }

    private void Movement() 
    {
        direction = new Vector3(Input.GetAxisRaw("Horizontal"), 0f, Input.GetAxisRaw("Vertical"));
        
        float velY = movement.y;
        movement = ((transform.forward * direction.z) + (transform.right * direction.x)).normalized;
        movement.y = velY;

        if (Input.GetButton("Fire3"))
        {
            actualSpeed = runSpeed;
        }
        else
        {
            actualSpeed = walkSpeed; 
        }
        if (IsGrounded())
        {
            movement.y = 0;
        }
        if (Input.GetButtonDown("Jump") && IsGrounded())
        {
            movement.y = jumpForce* Time.deltaTime;
        }

        movement.y += Physics.gravity.y * Time.deltaTime * gravityMod;
        controller.Move (movement * (actualSpeed * Time.deltaTime)); 
    }

    private bool IsGrounded()
    {
        isgrounded = false;

        if (Physics.SphereCast(transform.position + offset, radio, Vector3.down, out RaycastHit hit, distance, lm))
        {
            isgrounded = true;
        }

        return isgrounded;
    }
    #endregion
}

